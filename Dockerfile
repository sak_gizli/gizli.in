# Specify a base image
FROM node:12 as build-deps

# Create working directory and copy the app before running yarn install as the artifactory
# credentials can be inside .npmrc
RUN git clone https://gitlab.com/sak_gizli/gizli.in.git
WORKDIR /gizli.in/

# Run yarn install
RUN yarn install

# Build the project
CMD ["yarn", "run", "build"]

# Start the application
CMD yarn devel
